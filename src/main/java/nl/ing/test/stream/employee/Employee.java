package nl.ing.test.stream.employee;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;
import java.util.stream.Stream;

public class Employee {
    private String corp_id;
    private String name;
    private Gender gender;
    private LocalDate birthDate;
    private double salary;
    private Optional<Employee> manager;

    public Employee(String corp_id, String name, Gender gender, LocalDate birthDate, double salary, Employee manager) {
        this(corp_id, name, gender, birthDate, salary, Optional.of(manager));
    }

    public Employee(String corp_id, String name, Gender gender, LocalDate birthDate, double salary) {
        this(corp_id, name, gender, birthDate, salary, Optional.empty());
    }

    private Employee(String corp_id, String name, Gender gender, LocalDate birthDate, double salary, Optional<Employee> manager) {
        this.corp_id = corp_id;
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
        this.salary = salary;
        this.manager = manager;
    }

    public int getAge() {
        return Period.between(
                birthDate,
                LocalDate.of(1,7,2018) // Does not calculate actual age, but age on 1st of july 2018 (to keep the tests consistent)
        ).getYears();
    }

    public Stream<Employee> getManagers() {
        return Stream.empty();
    }

    public String getManagerName() {
        return "";
    }

    public String getCorpId() {
        return corp_id;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public double getSalary() {
        return salary;
    }

    public Optional<Employee> getManager() {
        return manager;
    }

    public String toString() {
        return name;
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        if (!(other instanceof Employee)) return false;
        return this.corp_id == ((Employee)other).corp_id;
    }

    @Override
    public int hashCode() {
        return corp_id.hashCode();
    }
}
