package nl.ing.test.stream.employee;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class EmployeeService {
    private List<Employee> employees;

    public EmployeeService(List employees) {
        this.employees = employees;
    }

    /**
     * findManagers - finds all managers (in any order) in the list of employees
     *
     * @return Set of managers
     */
    public Set<Employee> findManagers() {
        return Collections.EMPTY_SET;
    }

    public String getFirstLetters() {
        return "";
    }

    public Set<Employee> getEmployeesWithDirectManagerByGender(final Gender gender) {
        return Collections.EMPTY_SET;
    }

    public Set<Employee> getEmployeesHasManagerByGender(Gender gender) {
        return Collections.EMPTY_SET;
    }

    public Set<Employee> getEmployeesHasOnlyManagerByGender(Gender gender) {
        return Collections.EMPTY_SET;
    }


    public Optional<Employee> findEmployeeById(String corp_id) {
        return Optional.empty();
    }

    public Optional findManagerOfEmployeeById(String corp_id) {
        return Optional.empty();
    }
}
