package nl.ing.test.stream.employee;

public enum Gender {
    MALE,
    FEMALE
}
