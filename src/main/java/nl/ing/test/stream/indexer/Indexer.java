package nl.ing.test.stream.indexer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains functions for indexing words.
 */
public class Indexer {
    private final char[] letters;

    public Indexer(char... args){
        this.letters = args;
    }

    public Map<Integer, List<String>> getIndexes(final List<String> words) {
        Map<Integer, List<String>> result = new HashMap<>();
        for (String word: words) {
            int count = countLetters(word);
            if (!result.containsKey(count)) {
                result.put(count, new ArrayList<>());
            }
            List<String> wordList = result.get(count);
            wordList.add(word);
        }
        return result;

    }

    private int countLetters(String word) {
        int count = 0;
        for (int index = 0; index < word.length(); index++) {
            char c = word.charAt(index);
            for (int index2 = 0; index2 < letters.length; index2++) {
                if (letters[index2] == c) count++;
            }
        }
        return count;
    }
}