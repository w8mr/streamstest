package nl.ing.test.stream.indexer;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class IndexerTest {

    /**
     * Assignment: Index the words by the amount of vowels
     *
     */
    @Test
    public void testGetIndexesKlinkers() {
        Indexer indexer = new Indexer('a', 'e', 'i', 'o', 'u');

        List<String> woorden = Arrays.asList("one", "four", "man", "storm", "september");
        Map<Integer, List<String>> indexes = indexer.getIndexes(woorden);

        assertThat(indexes.get(1), containsInAnyOrder("man", "storm"));
        assertThat(indexes.get(2), containsInAnyOrder("one", "four"));
        assertThat(indexes.get(3), containsInAnyOrder("september"));
    }

    /**
     * Assignment: Index the words by the amount of non vowels
     *
     */
    @Test
    public void testGetIndexesMedeklinkers() {
        Indexer indexer = new Indexer('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z');

        List<String> woorden = Arrays.asList("one", "four", "man", "storm", "september");
        Map<Integer, List<String>> indexes = indexer.getIndexes(woorden);

        assertThat(indexes.get(1), containsInAnyOrder("one"));
        assertThat(indexes.get(2), containsInAnyOrder("four", "man"));
        assertThat(indexes.get(3), nullValue());
        assertThat(indexes.get(4), containsInAnyOrder("storm"));
        assertThat(indexes.get(5), nullValue());
        assertThat(indexes.get(6), containsInAnyOrder("september"));
    }
}