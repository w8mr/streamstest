package nl.ing.test.stream.employee;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static nl.ing.test.stream.employee.TestEmployees.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static nl.ing.test.stream.employee.Gender.*;


public class EmployeeServiceTest {
    EmployeeService service;

    @Before
    public void setup() {
        service = new EmployeeService(all_employees);
    }

    @Test
    public void testFindEmployeeById() {
        Optional employee = service.findEmployeeById("wp82qx");
        assertThat(employee.isPresent(), is(true));
        assertThat(employee.get(), is(lane_gill));
    }

    @Test
    public void testFindEmployeeByIdNonExisting() {
        Optional employee = service.findEmployeeById("ab12cd");
        assertThat(employee.isPresent(), is(false));
    }

    @Test
    public void testFindManagerOfEmployeeById() {
        Optional manager = service.findManagerOfEmployeeById("wp82qx");
        assertThat(manager.isPresent(), is(true));
        assertThat(manager.get(), is(brice_carr));
    }

    @Test
    public void testFindManagerOfEmployeeByIdNonExisting() {
        Optional manager = service.findManagerOfEmployeeById("ab12cd");
        assertThat(manager.isPresent(), is(false));
    }

    @Test
    public void testFindManagerOfEmployeeByIdNoManager() {
        Optional manager = service.findManagerOfEmployeeById("eb35me");
        assertThat(manager.isPresent(), is(false));
    }

    @Test
    public void testFindManagers() {
        Set<Employee> managers = service.findManagers();
        assertThat(managers, containsInAnyOrder(brice_carr, lane_black, harley_davidson, lane_gill, sammy_russell));
    }

    @Test
    public void testGetFirstLetters() {
        String firstLetters = service.getFirstLetters();
        assertThat(firstLetters, is("BLHLATBTJLSCT"));
    }

    @Test
    public void testGetEmployeesWithDirectManagerByGender() {
        Set<Employee> employees = service.getEmployeesWithDirectManagerByGender(MALE);
        assertThat(employees, containsInAnyOrder(lane_black, harley_davidson, lane_gill, angel_reynolds, tyler_mathis, lesley_mccormick, cory_spencer, taylor_sears));

        employees = service.getEmployeesWithDirectManagerByGender(FEMALE);
        assertThat(employees, containsInAnyOrder(bailey_erickson, terry_mclaughlin, jody_barber, sammy_russell));
    }

    @Test
    public void testGetEmployeesHasManagerByGender() {
        Set<Employee> employees = service.getEmployeesHasManagerByGender(MALE);
        assertThat(employees, containsInAnyOrder(lane_black, harley_davidson, lane_gill, angel_reynolds, tyler_mathis, lesley_mccormick, cory_spencer, taylor_sears, bailey_erickson, terry_mclaughlin, jody_barber, sammy_russell));

        employees = service.getEmployeesHasManagerByGender(FEMALE);
        assertThat(employees, containsInAnyOrder(bailey_erickson, terry_mclaughlin, jody_barber, sammy_russell, cory_spencer, taylor_sears));
    }

    @Test
    public void testGetEmployeesHasOnlyManagerByGender() {
        Set<Employee> employees = service.getEmployeesHasOnlyManagerByGender(MALE);
        assertThat(employees, containsInAnyOrder(lane_black, harley_davidson, lane_gill, angel_reynolds, tyler_mathis, lesley_mccormick));

        employees = service.getEmployeesHasOnlyManagerByGender(FEMALE);
        assertThat(employees, empty());
    }


}
