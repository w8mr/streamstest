package nl.ing.test.stream.employee;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static nl.ing.test.stream.employee.Gender.FEMALE;
import static nl.ing.test.stream.employee.Gender.MALE;

public class TestEmployees {
    static Employee brice_carr = new Employee("eb35me", "Brice Carr", MALE, LocalDate.of(1971, 1, 25), 134256.21);
    static Employee lane_black = new Employee("kq83ac", "Lane Black", FEMALE, LocalDate.of(1973, 3, 12), 104123.18, brice_carr);
    static Employee harley_davidson = new Employee("iw17mde", "Harley Davidson", MALE, LocalDate.of(1968, 2, 2), 88132.78, brice_carr);
    static Employee lane_gill = new Employee("wp82qx", "Lane Gill", FEMALE, LocalDate.of(1995, 11, 5), 98473.09, brice_carr);
    static Employee angel_reynolds = new Employee("ne96qa", "Angel Reynolds", FEMALE, LocalDate.of(1992, 2, 13), 43456.36, harley_davidson);
    static Employee tyler_mathis = new Employee("lk25vb", "Tyler Mathis", MALE, LocalDate.of(1986, 2, 17), 89567.93,harley_davidson);
    static Employee bailey_erickson = new Employee("pu77bn", "Bailey Erickson", FEMALE, LocalDate.of(1974, 9, 30), 66846.04, lane_black);
    static Employee terry_mclaughlin = new Employee("xr34vg", "Terry Mclaughlin", MALE, LocalDate.of(1975, 5, 31), 45274.83, lane_black);
    static Employee jody_barber = new Employee("hd44cs", "Jody Barber", FEMALE, LocalDate.of(1983, 10, 27), 33094.35, lane_gill);
    static Employee lesley_mccormick = new Employee("fv36hn", "Lesley Mccormick", FEMALE, LocalDate.of(1981, 3, 6), 57211.55, harley_davidson);
    static Employee sammy_russell = new Employee("kr82nb", "Sammy Russell", MALE, LocalDate.of(1991, 5, 8), 51968.85, lane_black);
    static Employee cory_spencer = new Employee("mm62lh", "Cory Spencer", FEMALE, LocalDate.of(1996, 12, 11), 34193.36, sammy_russell);
    static Employee taylor_sears = new Employee("yu09sx", "Taylor Sears", FEMALE, LocalDate.of(1983, 6, 16), 44883.23, sammy_russell);

    static List<Employee> all_employees = Arrays.asList(
            brice_carr,
            lane_black,
            harley_davidson,
            lane_gill,
            angel_reynolds,
            tyler_mathis,
            bailey_erickson,
            terry_mclaughlin,
            jody_barber,
            lesley_mccormick,
            sammy_russell,
            cory_spencer,
            taylor_sears);

}
