package nl.ing.test.stream.employee;

import org.junit.Test;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static nl.ing.test.stream.employee.TestEmployees.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class EmployeeTest {

    @Test
    public void testGetManagerName() {
        String name = lane_gill.getManagerName();
        assertThat(name, is("Brice Carr"));
    }

    @Test
    public void testGetManagerNameNoManager() {
        String name = brice_carr.getManagerName();
        assertThat(name, is("<No Manager>"));
    }

    @Test
    public void testGetManagers() {
        List<Employee> managers = taylor_sears.getManagers().collect(toList());
        assertThat(managers, contains(sammy_russell, lane_black, brice_carr));
    }


}
